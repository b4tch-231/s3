console.log("wew");

// To create a class in JS, we use the class keyword and {}

/* Syntax: 
class ClassName { 
  properties 
}; */

class Dog {

  // we add constructor method to a class to be able to initialize values upon instantiation of an object from a class
  constructor(name, breed, age){

    this.name = name;
    this.breed = breed;
    this.age = age * 7;
  }
}

// instantiation - process of creating objects
// an object created class is called an instance

let dog1 = new Dog("Puchi", "golden retriever", 0.6);
console.log(dog1);

 class Person {
  constructor(name, age, nationality, address){
    this.name = name;
    this.nationality = nationality;
    this.address = address;

    if(age >= 18){
      this.age = Number(age);
    } else {
      this.age = undefined;
    }
  }
  greet(){
    console.log(`Hello! Good Morning!`);
    return this;
  }
  introduce(){
    console.log(`My name is ${this.name}`);
    return this;
  }
  changeAddress(newAddress){
    this.address = newAddress;
    console.log(`${this.name} now lives in ${newAddress}`);
    return this;
  }
 }

 let person1 = new Person("Yin", 12, "Chinese", "Tokyo, Japan")
 let person2 = new Person("Yang", 26, "Vietnamese", "Manila, Philippines")

 console.log(person1);
 console.log(person2);

 /*
 1. class
 2. Uppercase
 3. new()
 4. instantiation
 5. constructor
*/

// chaining methods
class Student {
  constructor(name, email, grades){
    this.name = name;
    this.email = email;

    // check if first of the array has 4 elements
    if(grades.length === 4){
      if(grades.every(grade => grade >= 0 && grade <= 100)){
        this.grades = grades;
      } else {
        this.grades = undefined;
      }
    } else {
      this.grades = undefined;
    }

    this.gradeAve = undefined;
    this.passed = undefined;
    this.passedWithHonors = undefined;
  }
  // class methods - would be common to all instances.
  // make sure they are NOT separated by a comma
  login(){
    console.log(`${this.email} has logged in`);
    return this;
  }
  logout(email){
    console.log(`${email} has logged out`);
    return this;
  }
  listGrades(grades){
    this.grades.forEach(grade => {
      console.log(`${this.name}'s quarterly grade averages are: ${grade}`);
    })
    return this;
  }
  computeAve(){
    let sum = 0;
    this.grades.forEach(grade => sum = sum + grade);
    this.gradeAve = sum / 4;
    return this;
  }
  willPass(){
    this.passed = this.computeAve().gradeAve >= 85 ? true : false;
    return this;
  }
  willPassWithHonors(){
    if(this.passed){
      if(this.gradeAve >= 90){
        this.passedWithHonors = true;
      } else {
        this.passedWithHonors = false;
      }
    } else {
      this.passedWithHonors = false;
    }
    return this;
  }
 
}

// instantiate all four students from s2 using Student class

let studentOne = new Student("Tony", "starkisundustries@mail.com", [89, 84, 78, 88]);
let studentTwo = new Student("Peter", "spidey@mail.com", [78, 82, 79, 85]);
let studentThree = new Student("Wanda", "scarlettMaximoff@mail.com", [87, 89, 91, 93]);
let studentFour = new Student("Steve", "captainRogers@mail.com", [91, 89, 92, 93]);

/*
Activity #3:
    1. Should class methods be included in the class constructor?
        Answer: No


    2. Can class methods be separated by commas?
        Answer: No


    3. Can we update an object’s properties via dot notation?
        Answer: Yes


    4. What do you call the methods used to regulate access to an object’s properties?
        Answer:  Class Methods


    5. What does a method need to return in order for it to be chainable?
        Answer: 'this' keyword

*/